package httpresponse

import "github.com/gin-gonic/gin"

type ResponseError struct {
	Error string `json:"err"`
}

func BadRequest(ctx *gin.Context, msg string) {
	ctx.AbortWithStatusJSON(400, ResponseError{Error: msg})
}

func InternalErr(ctx *gin.Context, msg string) {
	ctx.AbortWithStatusJSON(500, ResponseError{Error: msg})
}
