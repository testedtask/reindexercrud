package main

import (
	"log"
	"os"
	"reindexercrud/internal/config"
	"reindexercrud/internal/database"
	"reindexercrud/internal/logger"
	"reindexercrud/internal/server"
	"reindexercrud/internal/usecase/authorusecase"
	"reindexercrud/internal/usecase/bookusecase"
	"reindexercrud/internal/usecase/publisherusecase"
)

func main() {
	cfgPath := os.Getenv("CONFIG_PATH")

	cfg, err := config.InitConfig(cfgPath)
	if err != nil {
		log.Fatal(err.Error())
	}

	lg := logger.InitLogger(cfg.Env)
	if lg == nil {
		log.Fatal("failed to init logger")
	}

	db, err := database.Connect(cfg.DatabaseURI, lg)
	if err != nil {
		log.Fatal(err.Error())
	}

	authorUsecase := authorusecase.New(db)
	bookUsecase := bookusecase.New(db)
	publisherUsecase := publisherusecase.New(db)

	srv := server.New(cfg.Addr, lg)

	srv.Start(authorUsecase, bookUsecase, publisherUsecase)
}
