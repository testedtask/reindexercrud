package entities

type Author struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	LastName string `json:"last_name"`

	BooksId []int   `json:"books_id"`
	Books   []*Book `json:"books,omitempty"`
}

type Book struct {
	Id          int          `json:"id"`
	AuthorId    []int        `json:"author_id"`
	Name        string       `json:"name"`
	Pages       int          `json:"pages"`
	Genres      []string     `json:"genres"`
	Sort        int          `json:"sort"`
	PublisherId []int        `json:"publisher_id"`
	Publishers  []*Publisher `json:"publishers,omitempty"`
}

type Publisher struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Sort int    `json:"sort"`
}
