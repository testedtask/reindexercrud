package authorusecase

import (
	"errors"
	"fmt"
	"reindexercrud/internal/database/models"
	"reindexercrud/internal/dto"
	"reindexercrud/internal/entities"
)

type Repository interface {
	GetAuthors(limit, page int) []models.Author
	GetJoinedAuthors(limit, page int) []models.Author
	GetAuthor(id int) models.Author
	GetJoinedAuthor(id int) models.Author
	CreateAuthor(models.Author) (int, error)
	AddAuthorToBook(author models.Author) error
	RemoveAuthorFromBook(author models.Author) error
	UpdateAuthor(models.Author) (int, error)
	DeleteAuthor(id int) error
}

type Usecase struct {
	repo Repository
}

func New(r Repository) Usecase {
	return Usecase{r}
}

func (u Usecase) GetAuthors(limit, page int) []entities.Author {
	authorModels := u.repo.GetAuthors(limit, page)
	authorsEnities := make([]entities.Author, 0, len(authorModels))

	for _, model := range authorModels {
		authorsEnities = append(authorsEnities, dto.AuthorModelToEntitie(model))
	}

	return authorsEnities
}

func (u Usecase) GetAuthorsInfo(limit, page int) []entities.Author {
	authorModels := u.repo.GetJoinedAuthors(limit, page)

	authorsEnities := make([]entities.Author, 0, len(authorModels))

	for _, model := range authorModels {
		authorsEnities = append(authorsEnities, dto.AuthorModelToEntitie(model))
	}

	return authorsEnities
}

func (u Usecase) GetAuthor(id int) (entities.Author, error) {
	author := u.repo.GetAuthor(id)
	if author.Id == 0 {
		return entities.Author{}, errors.New("author is not exist")
	}

	return dto.AuthorModelToEntitie(author), nil
}

func (u Usecase) GetAuthorInfo(id int) (entities.Author, error) {
	author := u.repo.GetJoinedAuthor(id)
	if author.Id == 0 {
		return entities.Author{}, errors.New("author is not exist")
	}

	return dto.AuthorModelToEntitie(author), nil
}

func (u Usecase) CreateAuthor(author entities.Author) (int, error) {
	authorModel := dto.AuthorEntitieToModel(author)

	id, err := u.repo.CreateAuthor(authorModel)
	if err != nil {
		return 0, fmt.Errorf("authorusecase.CreateAuthor: %w", err)
	}

	if id == 0 {
		return 0, fmt.Errorf("failed to create author")
	}

	authorModel.Id = id
	if err := u.repo.AddAuthorToBook(authorModel); err != nil {
		return 0, fmt.Errorf("bookusecase.CreateBook: %w", err)
	}

	return id, nil
}

func (u Usecase) UpdateAuthor(author entities.Author) error {
	oldAuthor := u.repo.GetAuthor(author.Id)
	if oldAuthor.Id == 0 {
		return errors.New("author is not exist")
	}

	if err := u.repo.RemoveAuthorFromBook(oldAuthor); err != nil {
		return fmt.Errorf("bookusecase.RemoveBookFromAuthors: %w", err)
	}

	authorModel := dto.AuthorEntitieToModel(author)

	n, err := u.repo.UpdateAuthor(authorModel)
	if err != nil {
		return fmt.Errorf("authorusecase.UpdateAuthor: %w", err)
	}

	if n == 0 {
		return fmt.Errorf("author is not exist")
	}

	if err := u.repo.AddAuthorToBook(authorModel); err != nil {
		return err
	}

	return nil
}

func (u Usecase) DeleteAuthor(id int) error {
	candidate := u.repo.GetAuthor(id)
	if candidate.Id == 0 {
		return errors.New("author is not exist")
	}

	if err := u.repo.RemoveAuthorFromBook(candidate); err != nil {
		return fmt.Errorf("publishersecase.DeleteBook: %w", err)
	}

	if err := u.repo.DeleteAuthor(id); err != nil {
		return fmt.Errorf("authorusecase.DeleteAuthor: %w", err)
	}

	return nil
}
