package bookusecase

import (
	"errors"
	"fmt"
	"reindexercrud/internal/database/models"
	"reindexercrud/internal/dto"
	"reindexercrud/internal/entities"
)

type Repository interface {
	GetBooks(limit, page int) []models.Book
	GetJoinedBooks(limit, page int) []models.Book
	GetBook(id int) models.Book
	GetJoinedBook(id int) models.Book
	CreateBook(models.Book) (int, error)
	RemoveBookFromAuthors(book models.Book) error
	AddBookToAuthors(book models.Book) error
	UpdateBook(models.Book) (int, error)
	DeleteBook(id int) error
}

type Usecase struct {
	repo Repository
}

func New(r Repository) Usecase {
	return Usecase{r}
}

func (u Usecase) GetBooks(limit, page int) []entities.Book {
	bookModels := u.repo.GetBooks(limit, page)
	booksEnities := make([]entities.Book, 0, len(bookModels))

	for _, model := range bookModels {
		booksEnities = append(booksEnities, dto.BookModelToEntitie(model))
	}

	return booksEnities
}

func (u Usecase) GetBooksInfo(limit, page int) []entities.Book {
	bookModels := u.repo.GetJoinedBooks(limit, page)

	booksEnities := make([]entities.Book, 0, len(bookModels))

	for _, model := range bookModels {
		booksEnities = append(booksEnities, dto.BookModelToEntitie(model))
	}

	return booksEnities
}

func (u Usecase) GetBook(id int) (entities.Book, error) {
	book := u.repo.GetBook(id)
	if book.Id == 0 {
		return entities.Book{}, errors.New("book is not exist")
	}

	return dto.BookModelToEntitie(book), nil
}

func (u Usecase) GetBookInfo(id int) (entities.Book, error) {
	bookModel := u.repo.GetJoinedBook(id)
	if bookModel.Id == 0 {
		return entities.Book{}, errors.New("book is not exist")
	}

	return dto.BookModelToEntitie(bookModel), nil
}

func (u Usecase) CreateBook(book entities.Book) (int, error) {
	bookModel := dto.BookEntitieToModel(book)

	id, err := u.repo.CreateBook(bookModel)
	if err != nil {
		return 0, fmt.Errorf("bookusecase.CreateBook: %w", err)
	}

	if id == 0 {
		return 0, fmt.Errorf("failed to create Book")
	}

	bookModel.Id = id
	if err := u.repo.AddBookToAuthors(bookModel); err != nil {
		return 0, fmt.Errorf("bookusecase.CreateBook: %w", err)
	}

	return id, nil
}

func (u Usecase) UpdateBook(book entities.Book) error {
	oldBook := u.repo.GetBook(book.Id)
	if oldBook.Id == 0 {
		return errors.New("book is not exist")
	}

	if err := u.repo.RemoveBookFromAuthors(oldBook); err != nil {
		return fmt.Errorf("bookusecase.RemoveBookFromAuthors: %w", err)
	}

	bookModel := dto.BookEntitieToModel(book)

	n, err := u.repo.UpdateBook(bookModel)
	if err != nil {
		return fmt.Errorf("bookusecase.UpdateBook: %w", err)
	}

	if n == 0 {
		return fmt.Errorf("book is not exist")
	}

	if err := u.repo.AddBookToAuthors(bookModel); err != nil {
		return err
	}

	return nil
}

func (u Usecase) DeleteBook(id int) error {
	candidate := u.repo.GetBook(id)
	if candidate.Id == 0 {
		return errors.New("book is not exist")
	}

	if err := u.repo.RemoveBookFromAuthors(candidate); err != nil {
		return fmt.Errorf("publishersecase.DeleteBook: %w", err)
	}

	if err := u.repo.DeleteBook(id); err != nil {
		return fmt.Errorf("publishersecase.DeleteBook: %w", err)
	}

	return nil
}
