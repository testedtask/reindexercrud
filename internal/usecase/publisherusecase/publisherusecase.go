package publisherusecase

import (
	"errors"
	"fmt"
	"reindexercrud/internal/database/models"
	"reindexercrud/internal/dto"
	"reindexercrud/internal/entities"
)

type Repository interface {
	GetPublishers(limit, page int) []models.Publisher
	GetPublisher(id int) models.Publisher
	CreatePublisher(publisher models.Publisher) (int, error)
	RemovePublisherFromBooks(publisher models.Publisher) error
	UpdatePublisher(publisher models.Publisher) (int, error)
	DeletePublisher(id int) error
}

type Usecase struct {
	repo Repository
}

func New(r Repository) Usecase {
	return Usecase{r}
}

func (u Usecase) GetPublishers(limit, page int) []entities.Publisher {
	publisherModels := u.repo.GetPublishers(limit, page)
	publisherEnities := make([]entities.Publisher, 0, len(publisherModels))

	for _, model := range publisherModels {
		publisherEnities = append(publisherEnities, dto.PublisherModelToEntitie(model))
	}

	return publisherEnities
}

func (u Usecase) GetPublisher(id int) (entities.Publisher, error) {
	publisher := u.repo.GetPublisher(id)
	if publisher.Id == 0 {
		return entities.Publisher{}, errors.New("publisher is not exist")
	}

	return dto.PublisherModelToEntitie(publisher), nil
}

func (u Usecase) CreatePublisher(publisher entities.Publisher) (int, error) {
	publisherModel := dto.PublisherEntitieToModel(publisher)

	id, err := u.repo.CreatePublisher(publisherModel)
	if err != nil {
		return 0, fmt.Errorf("publisherusecase.CreatePublisher: %w", err)
	}

	if id == 0 {
		return 0, fmt.Errorf("failed to create Publisher")
	}

	return id, nil
}

func (u Usecase) UpdatePublisher(publisher entities.Publisher) error {
	oldPublisher := u.repo.GetPublisher(publisher.Id)
	if oldPublisher.Id == 0 {
		return errors.New("publisher is not exist")
	}

	if err := u.repo.RemovePublisherFromBooks(oldPublisher); err != nil {
		return fmt.Errorf("publisherusecase.RemovePublisherFromAuthors: %w", err)
	}

	publisherModel := dto.PublisherEntitieToModel(publisher)

	n, err := u.repo.UpdatePublisher(publisherModel)
	if err != nil {
		return fmt.Errorf("publisherusecase.UpdatePublisher: %w", err)
	}

	if n == 0 {
		return fmt.Errorf("publisher is not exist")
	}

	return nil
}

func (u Usecase) DeletePublisher(id int) error {
	candidate := u.repo.GetPublisher(id)
	if candidate.Id == 0 {
		return errors.New("publisher is not exist")
	}

	if err := u.repo.RemovePublisherFromBooks(candidate); err != nil {
		return fmt.Errorf("publishersecase.DeletePublisher: %w", err)
	}

	if err := u.repo.DeletePublisher(id); err != nil {
		return fmt.Errorf("publishersecase.DeletePublisher: %w", err)
	}

	return nil
}
