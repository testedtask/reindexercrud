package models

type Author struct {
	Id       int    `reindex:"id,,pk"`
	Name     string `reindex:"name"`
	LastName string `reindex:"last_name"`

	BooksId []int   `reindex:"books_id"`
	Books   []*Book `reindex:"books,,joined"`
}

type Book struct {
	Id          int          `reindex:"id,,pk"`
	AuthorId    []int        `reindex:"author_id"`
	Name        string       `reindex:"name"`
	Pages       int          `reindex:"pages,tree"`
	Genres      []string     `reindex:"genres"`
	Sort        int          `reindex:"sort,tree"`
	PublisherId []int        `reindex:"publisher_id"`
	Publishers  []*Publisher `reindex:"publishers,,joined"`
}

type Publisher struct {
	Id   int    `reindex:"id,,pk"`
	Name string `reindex:"name"`
	Sort int    `reindex:"sort,tree"`
}
