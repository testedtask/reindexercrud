package database

import (
	"errors"
	"fmt"
	"log/slog"
	"reindexercrud/internal/database/models"
	"sync/atomic"

	"github.com/restream/reindexer"
)

const (
	AUTHOR_NAMESPACE    = "authors"
	BOOK_NAMESPACE      = "books"
	PUBLISHER_NAMESPACE = "publishers"
)

type Database struct {
	db          *reindexer.Reindexer
	log         *slog.Logger
	authorId    int64
	bookId      int64
	publisherId int64
}

func Connect(uri string, log *slog.Logger) (*Database, error) {
	db := reindexer.NewReindex(uri, reindexer.WithCreateDBIfMissing())

	if err := db.Status().Err; err != nil {
		return nil, fmt.Errorf("database.Connect: %w", err)
	}

	db.OpenNamespace("publishers", reindexer.DefaultNamespaceOptions(), models.Publisher{})
	db.OpenNamespace("books", reindexer.DefaultNamespaceOptions(), models.Book{})
	db.OpenNamespace("authors", reindexer.DefaultNamespaceOptions(), models.Author{})

	var (
		authorId    = 0
		bookId      = 0
		publisherId = 0
	)

	latestAuthor, found := db.Query(AUTHOR_NAMESPACE).
		Sort("id", true).
		Get()
	if found {
		if author, ok := latestAuthor.(*models.Author); ok {
			authorId = author.Id
		}
	}

	latestBook, found := db.Query(BOOK_NAMESPACE).
		Sort("id", true).
		Get()
	if found {
		if book, ok := latestBook.(*models.Book); ok {
			bookId = book.Id
		}
	}

	latestPublisher, found := db.Query(PUBLISHER_NAMESPACE).
		Sort("id", true).
		Get()
	if found {
		if publisher, ok := latestPublisher.(*models.Publisher); ok {
			publisherId = publisher.Id
		}
	}

	return &Database{
		db:          db,
		log:         log,
		authorId:    int64(authorId),
		bookId:      int64(bookId),
		publisherId: int64(publisherId),
	}, nil
}

// author repository

func (db *Database) GetAuthors(limit, page int) []models.Author {
	offset := limit * (page - 1)

	query := db.db.Query(AUTHOR_NAMESPACE).
		Limit(limit).
		Offset(offset)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	authors := make([]models.Author, 0, iterator.Count())
	for iterator.Next() {
		author, ok := iterator.Object().(*models.Author)
		if !ok {
			continue
		}
		authors = append(authors, *author)
	}
	return authors
}

func (db *Database) GetJoinedAuthors(limit, page int) []models.Author {
	offset := limit * (page - 1)

	query := db.db.Query(AUTHOR_NAMESPACE).
		Sort("id", false).
		Limit(limit).
		Offset(offset).
		Join(db.db.Query(BOOK_NAMESPACE), "books").
		On("books_id", reindexer.SET, "id").
		Sort("sort", true)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	booksIdSet := make(map[int]struct{})

	// scan result into arr and add required joined books into booksIdSet
	authors := make([]models.Author, 0, iterator.Count())
	for iterator.Next() {
		author, ok := iterator.Object().(*models.Author)
		if !ok {
			continue
		}

		authors = append(authors, *author)
		for _, id := range author.BooksId {
			booksIdSet[id] = struct{}{}
		}
	}

	booksId := make([]int, 0, len(booksIdSet))

	// all books ids that need to join
	for k := range booksIdSet {
		booksId = append(booksId, k)
	}

	// find books joined to publishers
	query = db.db.Query(BOOK_NAMESPACE).
		WhereInt("id", reindexer.SET, booksId...).
		Join(db.db.Query(PUBLISHER_NAMESPACE), "publishers").
		On("publisher_id", reindexer.EQ, "id")

	iterator = query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	// добавили все книги + паблишеры в мапу
	booksModels := make(map[int]*models.Book)
	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}
		booksModels[book.Id] = book
	}

	for _, author := range authors {
		for _, book := range author.Books {
			book.Publishers = booksModels[book.Id].Publishers
		}
	}

	return authors
}

func (db *Database) GetAuthor(id int) models.Author {
	author, found := db.db.Query(AUTHOR_NAMESPACE).
		WhereInt("id", reindexer.EQ, id).
		Get()

	if !found {
		return models.Author{}
	}

	autherModel, ok := author.(*models.Author)
	if !ok {
		return models.Author{}
	}

	return *autherModel
}

func (db *Database) GetJoinedAuthor(id int) models.Author {
	query := db.db.Query(AUTHOR_NAMESPACE).
		WhereInt("id", reindexer.EQ, id)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return models.Author{}
	}
	defer iterator.Close()

	var authorModel *models.Author

	for iterator.Next() {
		author, ok := iterator.Object().(*models.Author)
		if !ok {
			return models.Author{}
		}
		authorModel = author
	}

	if authorModel == nil {
		return models.Author{}
	}

	query = db.db.Query(BOOK_NAMESPACE).
		WhereInt("id", reindexer.SET, authorModel.BooksId...).
		Sort("sort", true).
		Join(db.db.Query(PUBLISHER_NAMESPACE), "publishers").
		On("publisher_id", reindexer.EQ, "id")

	iterator = query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return models.Author{}
	}
	defer iterator.Close()

	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}
		authorModel.Books = append(authorModel.Books, book)
	}

	return *authorModel
}

func (db *Database) CreateAuthor(author models.Author) (int, error) {
	author.Id = int(atomic.AddInt64(&db.authorId, 1))

	n, err := db.db.Insert(AUTHOR_NAMESPACE, author)
	if err != nil {
		return 0, err
	}
	if n == 0 {
		return 0, errors.New("internal err")
	}

	return author.Id, nil
}

func (db *Database) UpdateAuthor(author models.Author) (int, error) {
	return db.db.Update(AUTHOR_NAMESPACE, author)
}

func (db *Database) DeleteAuthor(id int) error {
	return db.db.Delete(AUTHOR_NAMESPACE, models.Author{Id: id})
}

func (db *Database) RemoveAuthorFromBook(author models.Author) error {
	tx, err := db.db.BeginTx(BOOK_NAMESPACE)
	if err != nil {
		tx.Rollback()
		return err
	}

	query := tx.Query().
		WhereInt("id", reindexer.SET, author.BooksId...)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		tx.Rollback()
		return nil
	}

	defer iterator.Close()

	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}
		updatedAuthorId := make([]int, 0, len(book.AuthorId)-1)

		for _, id := range book.AuthorId {
			if id != author.Id {
				updatedAuthorId = append(updatedAuthorId, id)
			}
		}

		book.AuthorId = updatedAuthorId
		tx.Update(book)
	}

	tx.Commit()

	return nil
}

func (db *Database) AddAuthorToBook(author models.Author) error {
	tx, err := db.db.BeginTx(BOOK_NAMESPACE)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("database.AddBookToAuthors: %w", err)
	}

	query := tx.Query().
		WhereInt("id", reindexer.SET, author.BooksId...).
		Not().
		WhereInt("author_id", reindexer.SET, author.Id)

	iterator := query.Exec()

	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		tx.Rollback()
		return fmt.Errorf("database.AddBookToAuthors: %w", iterator.Error())
	}

	defer iterator.Close()

	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}

		book.AuthorId = append(book.AuthorId, author.Id)

		tx.Update(book)
	}

	tx.Commit()

	return nil
}

// book repository

func (db *Database) GetBooks(limit, page int) []models.Book {
	offset := limit * (page - 1)

	query := db.db.Query(BOOK_NAMESPACE).
		Sort("id", false).
		Limit(limit).
		Offset(offset)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	books := make([]models.Book, 0, iterator.Count())
	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}
		books = append(books, *book)
	}

	return books
}

func (db *Database) GetJoinedBooks(limit, page int) []models.Book {
	offset := limit * (page - 1)

	// find books joined to publishers
	query := db.db.Query(BOOK_NAMESPACE).
		Sort("id", false).
		Limit(limit).
		Offset(offset).
		Join(db.db.Query(PUBLISHER_NAMESPACE), "publishers").
		On("publisher_id", reindexer.SET, "id").
		Sort("sort", true)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	books := make([]models.Book, 0, iterator.Count())

	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}

		books = append(books, *book)
	}

	return books
}

func (db *Database) GetBook(id int) models.Book {
	elem, found := db.db.Query(BOOK_NAMESPACE).
		WhereInt("id", reindexer.EQ, id).
		Get()

	if !found {
		return models.Book{}
	}

	bookModel, ok := elem.(*models.Book)
	if !ok {
		return models.Book{}
	}

	return *bookModel
}

func (db *Database) GetJoinedBook(id int) models.Book {
	elem, found := db.db.Query(BOOK_NAMESPACE).
		WhereInt("id", reindexer.EQ, id).
		Join(db.db.Query(PUBLISHER_NAMESPACE), "publishers").
		On("publisher_id", reindexer.SET, "id").
		Sort("sort", true).
		Get()

	if !found {
		return models.Book{}
	}

	book, ok := elem.(*models.Book)
	if !ok {
		return models.Book{}
	}

	return *book
}

func (db *Database) CreateBook(book models.Book) (int, error) {
	bookId := int(atomic.AddInt64(&db.bookId, 1))

	book.Id = bookId

	n, err := db.db.Insert(BOOK_NAMESPACE, book)
	if err != nil {
		return 0, err
	}

	if n == 0 {
		return 0, errors.New("internal err")
	}

	return book.Id, nil
}

func (db *Database) UpdateBook(book models.Book) (int, error) {
	return db.db.Update(BOOK_NAMESPACE, book)
}

func (db *Database) DeleteBook(id int) error {
	return db.db.Delete(BOOK_NAMESPACE, models.Book{Id: id})
}

func (db *Database) RemoveBookFromAuthors(book models.Book) error {
	tx, err := db.db.BeginTx(AUTHOR_NAMESPACE)
	if err != nil {
		tx.Rollback()
		return err
	}

	query := tx.Query().
		WhereInt("id", reindexer.SET, book.AuthorId...)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		tx.Rollback()
		return nil
	}

	defer iterator.Close()

	for iterator.Next() {
		author, ok := iterator.Object().(*models.Author)
		if !ok {
			continue
		}

		updatedBooksId := make([]int, 0, len(author.BooksId)-1)

		for _, id := range author.BooksId {
			if id != book.Id {
				updatedBooksId = append(updatedBooksId, id)
			}
		}

		author.BooksId = updatedBooksId
		tx.Update(author)
	}

	tx.Commit()

	return nil
}

func (db *Database) AddBookToAuthors(book models.Book) error {
	tx, err := db.db.BeginTx(AUTHOR_NAMESPACE)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("database.AddBookToAuthors: %w", err)
	}

	query := tx.Query().
		WhereInt("id", reindexer.SET, book.AuthorId...).
		Not().
		WhereInt("books_id", reindexer.SET, book.Id)

	iterator := query.Exec()

	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		tx.Rollback()
		return fmt.Errorf("database.AddBookToAuthors: %w", iterator.Error())
	}

	defer iterator.Close()

	for iterator.Next() {
		author, ok := iterator.Object().(*models.Author)
		if !ok {
			continue
		}

		author.BooksId = append(author.BooksId, book.Id)

		tx.Update(author)
	}

	tx.Commit()

	return nil
}

//publisher repository

func (db *Database) GetPublishers(limit, page int) []models.Publisher {
	offset := limit * (page - 1)

	query := db.db.Query(PUBLISHER_NAMESPACE).
		Sort("id", false).
		Limit(limit).
		Offset(offset)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		return nil
	}
	defer iterator.Close()

	publishers := make([]models.Publisher, 0, iterator.Count())

	for iterator.Next() {
		publisher, ok := iterator.Object().(*models.Publisher)
		if !ok {
			continue
		}
		publishers = append(publishers, *publisher)
	}

	return publishers
}

func (db *Database) GetPublisher(id int) models.Publisher {
	elem, found := db.db.Query(PUBLISHER_NAMESPACE).
		WhereInt("id", reindexer.EQ, id).
		Get()

	if !found {
		return models.Publisher{}
	}

	publisherModel, ok := elem.(*models.Publisher)
	if !ok {
		return models.Publisher{}
	}

	return *publisherModel
}

func (db *Database) CreatePublisher(publisher models.Publisher) (int, error) {
	pubId := int(atomic.AddInt64(&db.publisherId, 1))

	publisher.Id = pubId

	n, err := db.db.Insert(PUBLISHER_NAMESPACE, publisher)
	if err != nil {
		return 0, err
	}

	if n == 0 {
		return 0, errors.New("internal err")
	}

	return publisher.Id, nil
}

func (db *Database) UpdatePublisher(publisher models.Publisher) (int, error) {
	return db.db.Update(PUBLISHER_NAMESPACE, publisher)
}

func (db *Database) DeletePublisher(id int) error {
	return db.db.Delete(PUBLISHER_NAMESPACE, models.Publisher{Id: id})
}

func (db *Database) RemovePublisherFromBooks(publisher models.Publisher) error {
	tx, err := db.db.BeginTx(BOOK_NAMESPACE)
	if err != nil {
		tx.Rollback()
		return err
	}

	query := tx.Query().
		WhereInt("publisher_id", reindexer.SET, publisher.Id)

	iterator := query.Exec()
	if iterator.Error() != nil {
		db.log.Error(iterator.Error().Error())
		tx.Rollback()
		return nil
	}

	defer iterator.Close()

	for iterator.Next() {
		book, ok := iterator.Object().(*models.Book)
		if !ok {
			continue
		}

		updatedPublishersId := make([]int, 0, len(book.PublisherId)-1)

		for _, id := range book.PublisherId {
			if id != publisher.Id {
				updatedPublishersId = append(updatedPublishersId, id)
			}
		}

		book.PublisherId = updatedPublishersId
		tx.Update(book)
	}

	tx.Commit()

	return nil
}
