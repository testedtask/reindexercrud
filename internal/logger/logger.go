package logger

import (
	"log/slog"
	"os"
)

const (
	DEV  string = "DEV"
	PROD string = "PROD"
)

func InitLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case DEV:
		log = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case PROD:
		log = slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}))
	}

	return log
}
