package bookhandler

import (
	"reindexercrud/internal/entities"
	"reindexercrud/internal/server/response"
	"reindexercrud/pkg/httpresponse"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Usecase interface {
	GetBooks(limit, page int) []entities.Book
	GetBooksInfo(limit, page int) []entities.Book
	GetBook(id int) (entities.Book, error)
	GetBookInfo(id int) (entities.Book, error)
	CreateBook(book entities.Book) (int, error)
	UpdateBook(book entities.Book) error
	DeleteBook(id int) error
}

type Handler struct {
	usecase Usecase
}

func New(usecase Usecase) Handler {
	return Handler{
		usecase: usecase,
	}
}

// @Summary Получение информации о книгах
// @Tags BookController
// @Description Получение данных о книгах без данных о издательствах, в которых они выпускаются
// @Description Если параметры limit или page <= 0, то используется значение по умолчанию
// @Produce json
// @Param limit query int false "maximum documents in one page" default(100)
// @Param page query int false "page number in pagination" default(1)
// @Success 200 {array} entities.Book "Books data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books [get]
func (h Handler) GetBooks(c *gin.Context) {
	limit := 100
	page := 1

	limitParam := c.Query("limit")

	if limitParam != "" {
		if limitInt, err := strconv.Atoi(limitParam); err == nil {
			limit = limitInt
		}
	}

	pageParam := c.Query("page")

	if pageParam != "" {
		if pageInt, err := strconv.Atoi(pageParam); err == nil {
			page = pageInt
		}
	}

	books := h.usecase.GetBooks(limit, page)

	responseBooks := response.BooksEnitieToResponse(books)

	c.JSON(200, responseBooks)
}

// @Summary Получение полной информации о книгах
// @Tags BookController
// @Description Получение данных о книгах, включая инфорамцию о изданиях, в которых они выпускаются
// @Description Если параметры limit или page <= 0, то используется значение по умолчанию
// @Produce json
// @Param limit query int false "maximum documents in one page" default(100)
// @Param page query int false "page number in pagination" default(1)
// @Success 200 {array} entities.Book "Books data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books/info [get]
func (h Handler) GetBooksInfo(c *gin.Context) {
	limit := 100
	page := 1

	limitParam := c.Query("limit")

	if limitParam != "" {
		if limitInt, err := strconv.Atoi(limitParam); err == nil {
			limit = limitInt
		}
	}

	pageParam := c.Query("page")

	if pageParam != "" {
		if pageInt, err := strconv.Atoi(pageParam); err == nil {
			page = pageInt
		}
	}

	books := h.usecase.GetBooksInfo(limit, page)

	responseBooks := response.BooksEnitieToResponse(books)

	c.JSON(200, responseBooks)
}

// @Summary Получение информации о книге
// @Tags BookController
// @Description Получение данных о книге с id = {id}, без данных о изданиях, в которых она выпускается
// @Param id path uint true "Book id"
// @Produce json
// @Success 200 {object} entities.Book "Book's data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books/{id} [get]
func (h Handler) GetBook(c *gin.Context) {
	bookId, err := strconv.Atoi(c.Param("id"))
	if err != nil || bookId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	book, err := h.usecase.GetBook(bookId)
	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.JSON(200, book)
}

// @Summary Получение полной информации о книге
// @Tags BookController
// @Description Получение данных о кнгие с id = {id}, включая информацию о изданиях, в которых они издавались
// @Param id path uint true "Book id"
// @Produce json
// @Success 200 {object} entities.Book "Books data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books/{id}/info [get]
func (h Handler) GetBookInfo(c *gin.Context) {
	bookId, err := strconv.Atoi(c.Param("id"))
	if err != nil || bookId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	book, err := h.usecase.GetBookInfo(bookId)
	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.JSON(200, book)
}

type bookData struct {
	AuthorId    []int    `json:"author_id"`
	Name        string   `json:"name"`
	Pages       int      `json:"pages"`
	Genres      []string `json:"genres"`
	Sort        int      `json:"sort"`
	PublisherId []int    `json:"publisher_id"`
}

// @Summary Создание книги
// @Tags BookController
// @Description Создание новой книги со всеми атрибутами
// @Accept json
// @Produce json
// @Param request body bookhandler.bookData true "Book's data"
// @Success 201 {object} bookhandler.CreateBook.response "Book's id"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books [post]
func (h Handler) CreateBook(c *gin.Context) {
	var bookData bookData

	if err := c.ShouldBind(&bookData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	id, err := h.usecase.CreateBook(entities.Book{
		Id:          0,
		AuthorId:    bookData.AuthorId,
		Name:        bookData.Name,
		Pages:       bookData.Pages,
		Genres:      bookData.Genres,
		Sort:        bookData.Sort,
		PublisherId: bookData.PublisherId,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	type response struct {
		Id int `json:"id"`
	}

	c.JSON(201, response{id})
}

// @Summary Обновление книги
// @Tags BookController
// @Description Обновление информации о книге с id = {id}
// @Accept json
// @Produce json
// @Param id path uint true "Book id"
// @Param request body bookhandler.bookData true "Book's data"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books/{id} [put]
func (h Handler) UpdateBook(c *gin.Context) {
	bookId, err := strconv.Atoi(c.Param("id"))
	if err != nil || bookId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	var bookData bookData

	if err := c.ShouldBind(&bookData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	err = h.usecase.UpdateBook(entities.Book{
		Id:          bookId,
		AuthorId:    bookData.AuthorId,
		Name:        bookData.Name,
		Pages:       bookData.Pages,
		Genres:      bookData.Genres,
		Sort:        bookData.Sort,
		PublisherId: bookData.PublisherId,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}

// @Summary Удаление книги
// @Tags BookController
// @Description Удаление информации о книге с id = {id}
// @Param id path uint true "Book id"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /books/{id} [delete]
func (h Handler) DeleteBook(c *gin.Context) {
	bookId, err := strconv.Atoi(c.Param("id"))
	if err != nil || bookId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	err = h.usecase.DeleteBook(bookId)

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}
