package authorhandler

import (
	"reindexercrud/internal/entities"
	"reindexercrud/internal/server/response"
	"reindexercrud/pkg/httpresponse"
	"strconv"

	"github.com/gin-gonic/gin"
)

//go:generate go run github.com/vektra/mockery/v2@v2.39.1 --name=Usecase
type Usecase interface {
	GetAuthors(limit, page int) []entities.Author
	GetAuthorsInfo(limit, page int) []entities.Author
	GetAuthor(id int) (entities.Author, error)
	GetAuthorInfo(id int) (entities.Author, error)
	CreateAuthor(entities.Author) (int, error)
	UpdateAuthor(entities.Author) error
	DeleteAuthor(id int) error
}

type Handler struct {
	usecase Usecase
}

func New(usecase Usecase) Handler {
	return Handler{
		usecase: usecase,
	}
}

// @Summary Получение информации об авторах
// @Tags AuthorController
// @Description Получение данных о авторах без данных о книгах, которые написали данные авторы и изданий, в которых эти книги выпускались
// @Description Если параметры limit или page <= 0, то используется значение по умолчанию
// @Produce json
// @Param limit query int false "maximum documents in one page" default(100)
// @Param page query int false "page number in pagination" default(1)
// @Success 200 {array} response.Author "Authors data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors [get]
func (h Handler) GetAuthors(c *gin.Context) {
	limit := 100
	page := 1

	limitParam := c.Query("limit")

	if limitParam != "" {
		if limitInt, err := strconv.Atoi(limitParam); err == nil && limitInt > 0 {
			limit = limitInt
		}
	}

	pageParam := c.Query("page")

	if pageParam != "" {
		if pageInt, err := strconv.Atoi(pageParam); err == nil && pageInt > 0 {
			page = pageInt
		}
	}

	authors := h.usecase.GetAuthors(limit, page)

	responseAuthors := response.AuthorsEntitieToResponse(authors)

	c.JSON(200, responseAuthors)
}

// @Summary Получение полной информации об авторах
// @Tags AuthorController
// @Description Получение данных о авторах, включая информацию о книгах, которые написали авторы и изданиях, в которых они издавались
// @Description Если параметры limit или page <= 0, то используется значение по умолчанию
// @Produce json
// @Param limit query int false "maximum documents in one page" default(100)
// @Param page query int false "page number in pagination" default(1)
// @Success 200 {array} response.Author "Authors data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors/info [get]
func (h Handler) GetAuthorsInfo(c *gin.Context) {
	limit := 100
	page := 1

	limitParam := c.Query("limit")

	if limitParam != "" {
		if limitInt, err := strconv.Atoi(limitParam); err == nil {
			limit = limitInt
		}
	}

	pageParam := c.Query("page")

	if pageParam != "" {
		if pageInt, err := strconv.Atoi(pageParam); err == nil {
			page = pageInt
		}
	}

	authors := h.usecase.GetAuthorsInfo(limit, page)

	responseAuthors := response.AuthorsEntitieToResponse(authors)

	c.JSON(200, responseAuthors)
}

// @Summary Получение информации об авторе
// @Tags AuthorController
// @Description Получение данных об авторе с id = {id}, без данных о книгах, которые написал автор и изданиях, в которых они издавались
// @Param id path uint true "Author id"
// @Produce json
// @Success 200 {object} entities.Author "Authors data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors/{id} [get]
func (h Handler) GetAuthor(c *gin.Context) {
	authorId, err := strconv.Atoi(c.Param("id"))
	if err != nil || authorId <= 0 {
		httpresponse.BadRequest(c, "invalid author id")
		return
	}

	author, err := h.usecase.GetAuthor(authorId)
	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.JSON(200, author)
}

// @Summary Получение полной информации об авторе
// @Tags AuthorController
// @Description Получение данных об авторе с id = {id}, включая информацию о книгах, которые написал автор и изданиях, в которых они издавались
// @Param id path uint true "Author id"
// @Produce json
// @Success 200 {object} entities.Author "Authors data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors/{id}/info [get]
func (h Handler) GetAuthorInfo(c *gin.Context) {
	authorId, err := strconv.Atoi(c.Param("id"))
	if err != nil || authorId <= 0 {
		httpresponse.BadRequest(c, "invalid author id")
		return
	}

	author, err := h.usecase.GetAuthorInfo(authorId)
	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.JSON(200, author)
}

type AuthorData struct {
	Name     string `json:"name"`
	LastName string `json:"last_name"`
	BooksId  []int  `json:"books_id"`
}

// @Summary Создание автора
// @Tags AuthorController
// @Description Создание нового автора со всеми атрибутами
// @Accept json
// @Produce json
// @Param request body authorhandler.AuthorData true "Author's data"
// @Success 201 {object} authorhandler.CreateAuthor.response "Client's id"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors [post]
func (h Handler) CreateAuthor(c *gin.Context) {
	var authorData AuthorData

	if err := c.ShouldBind(&authorData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	id, err := h.usecase.CreateAuthor(entities.Author{
		Id:       0,
		LastName: authorData.LastName,
		BooksId:  authorData.BooksId,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	type response struct {
		Id int `json:"id"`
	}

	c.JSON(201, response{id})
}

// @Summary Обновление автора
// @Tags AuthorController
// @Description Обновление информации о авторе с id = {id}
// @Accept json
// @Produce json
// @Param id path uint true "Author id"
// @Param request body authorhandler.AuthorData true "Author's data"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors/{id} [put]
func (h Handler) UpdateAuthor(c *gin.Context) {
	authorId, err := strconv.Atoi(c.Param("id"))
	if err != nil || authorId <= 0 {
		httpresponse.BadRequest(c, "invalid author id")
		return
	}

	var authorData AuthorData

	if err := c.ShouldBind(&authorData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	err = h.usecase.UpdateAuthor(entities.Author{
		Id:       authorId,
		LastName: authorData.LastName,
		BooksId:  authorData.BooksId,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}

// @Summary Удаление автора
// @Tags AuthorController
// @Description Удаление информации о авторе с id = {id}
// @Param id path uint true "Author id"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /authors/{id} [delete]
func (h Handler) DeleteAuthor(c *gin.Context) {
	authorId, err := strconv.Atoi(c.Param("id"))
	if err != nil || authorId <= 0 {
		httpresponse.BadRequest(c, "invalid author id")
		return
	}

	err = h.usecase.DeleteAuthor(authorId)

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}
