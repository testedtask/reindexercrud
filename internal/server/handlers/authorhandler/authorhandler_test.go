package authorhandler

import (
	"errors"
	"fmt"
	"net/http/httptest"
	"reindexercrud/internal/entities"
	"reindexercrud/internal/server/handlers/authorhandler/mocks"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandler_GetAuthors(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		limit        int
		page         int
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:  "OK",
			limit: 100,
			page:  1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthors", 100, 1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
						{
							Id:       2,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1,2]},{"id":2,"last_name":"Doe","books_id":[1,2]}]`,
		},
		{
			name:  "Invalid limit param",
			limit: -100,
			page:  1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthors", 100, 1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
						{
							Id:       2,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1,2]},{"id":2,"last_name":"Doe","books_id":[1,2]}]`,
		},
		{
			name:  "Invalid page param",
			limit: 100,
			page:  -1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthors", 100, 1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
						{
							Id:       2,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1, 2},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1,2]},{"id":2,"last_name":"Doe","books_id":[1,2]}]`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.GET("/authors", handler.GetAuthors)

			requestUrl := fmt.Sprintf("/authors?limit=%d&page=%d", tt.limit, tt.page)

			req := httptest.NewRequest("GET", requestUrl, nil)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_GetAuthorsInfo(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		limit        int
		page         int
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:  "OK",
			limit: 100,
			page:  1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthorsInfo", 100, 1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1},
							Books: []*entities.Book{
								{
									Id:          1,
									AuthorId:    []int{1},
									Name:        "Text",
									Pages:       10,
									Genres:      []string{"genre1"},
									Sort:        10,
									PublisherId: []int{1},
									Publishers: []*entities.Publisher{
										{
											Id:   1,
											Name: "Foo",
											Sort: 2,
										},
									},
								},
							},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1],"books":[{"id":1,"author_id":[1],"name":"Text","sort":10,"publisher_id":[1],"publishers":[{"id":1,"name":"Foo"}]}]}]`,
		},
		{
			name:  "Invalid limit param",
			limit: -100,
			page:  1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthorsInfo", -100, 1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1},
							Books: []*entities.Book{
								{
									Id:          1,
									AuthorId:    []int{1},
									Name:        "Text",
									Pages:       10,
									Genres:      []string{"genre1"},
									Sort:        10,
									PublisherId: []int{1},
									Publishers: []*entities.Publisher{
										{
											Id:   1,
											Name: "Foo",
											Sort: 2,
										},
									},
								},
							},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1],"books":[{"id":1,"author_id":[1],"name":"Text","sort":10,"publisher_id":[1],"publishers":[{"id":1,"name":"Foo"}]}]}]`,
		},
		{
			name:  "Invalid page param",
			limit: 100,
			page:  -1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthorsInfo", 100, -1).
					Once().
					Return([]entities.Author{
						{
							Id:       1,
							Name:     "John",
							LastName: "Doe",
							BooksId:  []int{1},
							Books: []*entities.Book{
								{
									Id:          1,
									AuthorId:    []int{1},
									Name:        "Text",
									Pages:       10,
									Genres:      []string{"genre1"},
									Sort:        10,
									PublisherId: []int{1},
									Publishers: []*entities.Publisher{
										{
											Id:   1,
											Name: "Foo",
											Sort: 2,
										},
									},
								},
							},
						},
					})
			},
			expectedCode: 200,
			expectedBody: `[{"id":1,"last_name":"Doe","books_id":[1],"books":[{"id":1,"author_id":[1],"name":"Text","sort":10,"publisher_id":[1],"publishers":[{"id":1,"name":"Foo"}]}]}]`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.GET("/authors/info", handler.GetAuthorsInfo)

			requestUrl := fmt.Sprintf("/authors/info?limit=%d&page=%d", tt.limit, tt.page)

			req := httptest.NewRequest("GET", requestUrl, nil)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_GetAuthor(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		inputId      int
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:    "OK",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthor", 1).
					Once().
					Return(entities.Author{
						Id:       1,
						Name:     "John",
						LastName: "Doe",
						BooksId:  []int{1},
					}, nil)
			},
			expectedCode: 200,
			expectedBody: `{"id":1,"name":"John","last_name":"Doe","books_id":[1]}`,
		},
		{
			name:         "Invalid id param",
			inputId:      -1,
			expectedCode: 400,
			expectedBody: `{"err":"invalid author id"}`,
		},
		{
			name:    "Usecase error",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthor", 1).
					Once().
					Return(entities.Author{}, errors.New("something goes wrong"))
			},
			expectedCode: 400,
			expectedBody: `{"err":"something goes wrong"}`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.GET("/authors/:id", handler.GetAuthor)

			requestUrl := fmt.Sprintf("/authors/%d", tt.inputId)

			req := httptest.NewRequest("GET", requestUrl, nil)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_GetAuthorInfo(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		inputId      int
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:    "OK",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthorInfo", 1).
					Once().
					Return(entities.Author{
						Id:       1,
						Name:     "John",
						LastName: "Doe",
						BooksId:  []int{1},
						Books: []*entities.Book{{
							Id:          1,
							AuthorId:    []int{1},
							Name:        "text",
							Pages:       10,
							Genres:      []string{"genre1"},
							Sort:        1,
							PublisherId: []int{1},
							Publishers: []*entities.Publisher{
								{
									Id:   1,
									Name: "Foo",
								},
							},
						}},
					}, nil)
			},
			expectedCode: 200,
			expectedBody: `{"id":1,"name":"John","last_name":"Doe","books_id":[1],"books":[{"id":1,"author_id":[1],"name":"text","pages":10,"genres":["genre1"],"sort":1,"publisher_id":[1],"publishers":[{"id":1,"name":"Foo","sort":0}]}]}`,
		},
		{
			name:         "Invalid id param",
			inputId:      -1,
			expectedCode: 400,
			expectedBody: `{"err":"invalid author id"}`,
		},
		{
			name:    "Usecase error",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("GetAuthorInfo", 1).
					Once().
					Return(entities.Author{}, errors.New("something goes wrong"))
			},
			expectedCode: 400,
			expectedBody: `{"err":"something goes wrong"}`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.GET("/authors/:id/info", handler.GetAuthorInfo)

			requestUrl := fmt.Sprintf("/authors/%d/info", tt.inputId)

			req := httptest.NewRequest("GET", requestUrl, nil)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_CreateAuthor(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		inputBody    string
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:      "OK",
			inputBody: `{"name":"John","last_name":"Doe","books_id":[1]}`,
			prepareMock: func(f *fields) {
				f.mock.On("CreateAuthor", mock.Anything).
					Once().
					Return(1, nil)
			},
			expectedCode: 201,
			expectedBody: `{"id":1}`,
		},
		{
			name:      "Usecase error",
			inputBody: `{"name":"John","last_name":"Doe","books_id":[1]}`,
			prepareMock: func(f *fields) {
				f.mock.On("CreateAuthor", mock.Anything).
					Once().
					Return(0, errors.New("something goes wrong"))
			},
			expectedCode: 400,
			expectedBody: `{"err":"something goes wrong"}`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.POST("/authors", handler.CreateAuthor)

			requestUrl := fmt.Sprintf("/authors")

			req := httptest.NewRequest("POST", requestUrl, strings.NewReader(tt.inputBody))

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_UpdateAuthor(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		inputId      int
		inputBody    string
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:      "OK",
			inputId:   1,
			inputBody: `{"name":"John","last_name":"Doe","books_id":[1]}`,
			prepareMock: func(f *fields) {
				f.mock.On("UpdateAuthor", mock.Anything).
					Once().
					Return(nil)
			},
			expectedCode: 200,
		},
		{
			name:         "Invalid author id",
			inputId:      -1,
			inputBody:    `{"name":"John","last_name":"Doe","books_id":[1]}`,
			expectedCode: 400,
			expectedBody: `{"err":"invalid author id"}`,
		},
		{
			name:      "Usecase error",
			inputId:   1,
			inputBody: `{"name":"John","last_name":"Doe","books_id":[1]}`,
			prepareMock: func(f *fields) {
				f.mock.On("UpdateAuthor", mock.Anything).
					Once().
					Return(errors.New("something goes wrong"))
			},
			expectedCode: 400,
			expectedBody: `{"err":"something goes wrong"}`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.PUT("/authors/:id", handler.UpdateAuthor)

			requestUrl := fmt.Sprintf("/authors/%d", tt.inputId)

			req := httptest.NewRequest("PUT", requestUrl, strings.NewReader(tt.inputBody))

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}

func TestHandler_DeleteAuthor(t *testing.T) {
	type fields struct {
		mock *mocks.Usecase
	}

	testTable := []struct {
		name         string
		inputId      int
		prepareMock  func(f *fields)
		expectedCode int
		expectedBody string
	}{
		{
			name:    "OK",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("DeleteAuthor", mock.Anything).
					Once().
					Return(nil)
			},
			expectedCode: 200,
		},
		{
			name:         "Invalid author id",
			inputId:      -1,
			expectedCode: 400,
			expectedBody: `{"err":"invalid author id"}`,
		},
		{
			name:    "Usecase error",
			inputId: 1,
			prepareMock: func(f *fields) {
				f.mock.On("DeleteAuthor", mock.Anything).
					Once().
					Return(errors.New("something goes wrong"))
			},
			expectedCode: 400,
			expectedBody: `{"err":"something goes wrong"}`,
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {
			r := gin.New()

			f := fields{
				mock: mocks.NewUsecase(t),
			}
			if tt.prepareMock != nil {
				tt.prepareMock(&f)
			}

			handler := Handler{
				usecase: f.mock,
			}

			r.DELETE("/authors/:id", handler.DeleteAuthor)

			requestUrl := fmt.Sprintf("/authors/%d", tt.inputId)

			req := httptest.NewRequest("DELETE", requestUrl, nil)

			w := httptest.NewRecorder()
			r.ServeHTTP(w, req)

			assert.Equal(t, tt.expectedCode, w.Code)

			assert.Equal(t, tt.expectedBody, w.Body.String())
		})
	}
}
