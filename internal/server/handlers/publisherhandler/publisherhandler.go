package publisherhandler

import (
	"reindexercrud/internal/entities"
	"reindexercrud/internal/server/response"
	"reindexercrud/pkg/httpresponse"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Usecase interface {
	GetPublishers(limit, page int) []entities.Publisher
	GetPublisher(id int) (entities.Publisher, error)
	CreatePublisher(entities.Publisher) (int, error)
	UpdatePublisher(entities.Publisher) error
	DeletePublisher(id int) error
}

type Handler struct {
	usecase Usecase
}

func New(usecase Usecase) Handler {
	return Handler{
		usecase: usecase,
	}
}

// @Summary Получение информации о издательствах
// @Tags PublisherController
// @Description Получение данных об издательствах
// @Description Если параметры limit или page <= 0, то используется значение по умолчанию
// @Produce json
// @Param limit query int false "maximum documents in one page" default(100)
// @Param page query int false "page number in pagination" default(1)
// @Success 200 {array} entities.Publisher "Publishers data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /publishers [get]
func (h Handler) GetPublishers(c *gin.Context) {
	limit := 100
	page := 1

	limitParam := c.Query("limit")

	if limitParam != "" {
		if limitInt, err := strconv.Atoi(limitParam); err == nil {
			limit = limitInt
		}
	}

	pageParam := c.Query("page")

	if pageParam != "" {
		if pageInt, err := strconv.Atoi(pageParam); err == nil {
			page = pageInt
		}
	}

	publishers := h.usecase.GetPublishers(limit, page)

	responsePublishers := response.PublishersEntitiesToResponse(publishers)

	c.JSON(200, responsePublishers)
}

// @Summary Получение информации об издательстве
// @Tags PublisherController
// @Description Получение данных об издательстве с id = {id}
// @Param id path uint true "Publisher id"
// @Produce json
// @Success 200 {object} entities.Publisher "Publisher's data"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /publishers/{id} [get]
func (h Handler) GetPublisher(c *gin.Context) {
	publisherId, err := strconv.Atoi(c.Param("id"))
	if err != nil || publisherId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	piblisher, err := h.usecase.GetPublisher(publisherId)
	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.JSON(200, piblisher)
}

type publisherData struct {
	Name string `json:"name"`
	Sort int    `json:"sort"`
}

// @Summary Создание издательства
// @Tags PublisherController
// @Description Создание новой нового издательства со всеми атрибутами
// @Accept json
// @Produce json
// @Param request body publisherhandler.publisherData true "Publisher's data"
// @Success 201 {object} publisherhandler.CreatePublisher.response "Publisher's id"
// @Failure 400 {object} httpresponse.ResponseError
// @Router /publishers [post]
func (h Handler) CreatePublisher(c *gin.Context) {
	var pubData publisherData

	if err := c.ShouldBind(&pubData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	id, err := h.usecase.CreatePublisher(entities.Publisher{
		Id:   0,
		Name: pubData.Name,
		Sort: pubData.Sort,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	type response struct {
		Id int `json:"id"`
	}

	c.JSON(201, response{id})
}

// @Summary Обновление издательства
// @Tags PublisherController
// @Description Обновление информации об издательстве с id = {id}
// @Accept json
// @Produce json
// @Param id path uint true "Publisher id"
// @Param request body publisherhandler.publisherData true "Publisher's data"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /publishers/{id} [put]
func (h Handler) UpdatePublisher(c *gin.Context) {
	publisherId, err := strconv.Atoi(c.Param("id"))
	if err != nil || publisherId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	var pubData publisherData

	if err := c.ShouldBind(&pubData); err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	err = h.usecase.UpdatePublisher(entities.Publisher{
		Id:   publisherId,
		Name: pubData.Name,
		Sort: pubData.Sort,
	})

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}

// @Summary Удаление издательства
// @Tags PublisherController
// @Description Удаление информации об издательстве с id = {id}
// @Param id path uint true "Publisher id"
// @Success 200
// @Failure 400 {object} httpresponse.ResponseError
// @Router /publishers/{id} [delete]
func (h Handler) DeletePublisher(c *gin.Context) {
	bookId, err := strconv.Atoi(c.Param("id"))
	if err != nil || bookId <= 0 {
		httpresponse.BadRequest(c, "invalid book id")
		return
	}

	err = h.usecase.DeletePublisher(bookId)

	if err != nil {
		httpresponse.BadRequest(c, err.Error())
		return
	}

	c.Status(200)
}
