package response

import (
	"reindexercrud/internal/entities"
	"sync"
)

type Author struct {
	Id       int    `json:"id"`
	LastName string `json:"last_name"`

	BooksId []int  `json:"books_id"`
	Books   []Book `json:"books,omitempty"`
}

type Book struct {
	Id          int         `json:"id"`
	AuthorId    []int       `json:"author_id"`
	Name        string      `json:"name"`
	Sort        int         `json:"sort"`
	PublisherId []int       `json:"publisher_id"`
	Publishers  []Publisher `json:"publishers,omitempty"`
}

type Publisher struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func AuthorsEntitieToResponse(authors []entities.Author) []Author {
	responseAuthors := make([]Author, len(authors))

	var authorWg sync.WaitGroup

	for i, author := range authors {
		authorWg.Add(1)

		go func(index int, a entities.Author) {
			defer authorWg.Done()

			responseBooks := make([]Book, len(a.Books))

			var bookWg sync.WaitGroup

			for i, book := range a.Books {
				bookWg.Add(1)

				go func(j int, b *entities.Book) {
					defer bookWg.Done()

					var pubWg sync.WaitGroup

					responsePublishers := make([]Publisher, len(b.Publishers))

					for i, pub := range b.Publishers {
						pubWg.Add(1)

						go func(k int, p *entities.Publisher) {
							pubWg.Done()

							responsePublishers[k] = Publisher{
								Id:   p.Id,
								Name: p.Name,
							}

						}(i, pub)
					}

					pubWg.Wait()

					responseBooks[j] = Book{
						Id:          b.Id,
						AuthorId:    b.AuthorId,
						Name:        b.Name,
						Sort:        b.Sort,
						PublisherId: b.PublisherId,
						Publishers:  responsePublishers,
					}
				}(i, book)
			}

			bookWg.Wait()

			responseAuthors[index] = Author{
				Id:       a.Id,
				LastName: a.LastName,
				BooksId:  a.BooksId,
				Books:    responseBooks,
			}
		}(i, author)
	}

	authorWg.Wait()

	return responseAuthors
}

func BooksEnitieToResponse(books []entities.Book) []Book {
	responseBooks := make([]Book, len(books))

	var bookWg sync.WaitGroup

	for i, book := range books {
		bookWg.Add(1)

		go func(j int, b entities.Book) {
			defer bookWg.Done()

			var authorWg sync.WaitGroup

			responsePublishers := make([]Publisher, len(b.Publishers))

			for i, pub := range b.Publishers {
				authorWg.Add(1)

				go func(k int, p *entities.Publisher) {
					authorWg.Done()

					responsePublishers[k] = Publisher{
						Id:   p.Id,
						Name: p.Name,
					}

				}(i, pub)
			}

			authorWg.Wait()

			responseBooks[j] = Book{
				Id:          b.Id,
				AuthorId:    b.AuthorId,
				Name:        b.Name,
				Sort:        b.Sort,
				PublisherId: b.PublisherId,
				Publishers:  responsePublishers,
			}
		}(i, book)
	}

	bookWg.Wait()

	return responseBooks
}

func PublishersEntitiesToResponse(publishers []entities.Publisher) []Publisher {
	responsePublishers := make([]Publisher, len(publishers))

	var wg sync.WaitGroup

	for i, pub := range publishers {
		wg.Add(1)

		go func(j int, p entities.Publisher) {
			wg.Done()

			responsePublishers[j] = Publisher{
				Id:   p.Id,
				Name: p.Name,
			}

		}(i, pub)
	}

	wg.Wait()

	return responsePublishers
}
