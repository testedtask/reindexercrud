package response

import (
	"reindexercrud/internal/entities"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAuthorsEntitieToResponse(t *testing.T) {
	testTable := []struct {
		name           string
		inputData      []entities.Author
		expectedResult []Author
	}{
		{
			name: "OK",
			inputData: []entities.Author{
				{
					Id:       1,
					Name:     "John",
					LastName: "Doe",
					BooksId:  []int{1, 2},
					Books: []*entities.Book{
						{
							Id:          1,
							AuthorId:    []int{1},
							Name:        "Text",
							Pages:       10,
							Genres:      []string{"genre1"},
							Sort:        10,
							PublisherId: []int{1, 2},
							Publishers: []*entities.Publisher{
								{
									Id:   1,
									Name: "Foo",
									Sort: 2,
								},
								{
									Id:   2,
									Name: "Bar",
									Sort: 1,
								},
							},
						},
						{
							Id:          2,
							AuthorId:    []int{1},
							Name:        "Text",
							Pages:       10,
							Genres:      []string{"genre2"},
							Sort:        1,
							PublisherId: []int{2},
							Publishers: []*entities.Publisher{
								{
									Id:   2,
									Name: "Bar",
									Sort: 1,
								},
							},
						},
					},
				},
				{
					Id:       2,
					Name:     "John2",
					LastName: "Doe2",
					BooksId:  []int{3, 4},
					Books: []*entities.Book{
						{
							Id:          3,
							AuthorId:    []int{1},
							Name:        "Text",
							Pages:       10,
							Genres:      []string{"genre3"},
							Sort:        10,
							PublisherId: []int{1},
							Publishers: []*entities.Publisher{
								{
									Id:   1,
									Name: "Foo",
								},
							},
						},
						{
							Id:          4,
							AuthorId:    []int{1},
							Name:        "Text",
							Pages:       10,
							Genres:      []string{"genre4"},
							Sort:        1,
							PublisherId: []int{2},
							Publishers: []*entities.Publisher{
								{
									Id:   2,
									Name: "Bar",
									Sort: 4,
								},
							},
						},
					},
				},
			},
			expectedResult: []Author{
				{
					Id:       1,
					LastName: "Doe",
					BooksId:  []int{1, 2},
					Books: []Book{
						{
							Id:          1,
							AuthorId:    []int{1},
							Name:        "Text",
							Sort:        10,
							PublisherId: []int{1, 2},
							Publishers: []Publisher{
								{
									Id:   1,
									Name: "Foo",
								},
								{
									Id:   2,
									Name: "Bar",
								},
							},
						},
						{
							Id:          2,
							AuthorId:    []int{1},
							Name:        "Text",
							Sort:        1,
							PublisherId: []int{2},
							Publishers: []Publisher{
								{
									Id:   2,
									Name: "Bar",
								},
							},
						},
					},
				},
				{
					Id:       2,
					LastName: "Doe2",
					BooksId:  []int{3, 4},
					Books: []Book{
						{
							Id:          3,
							AuthorId:    []int{1},
							Name:        "Text",
							Sort:        10,
							PublisherId: []int{1},
							Publishers: []Publisher{
								{
									Id:   1,
									Name: "Foo",
								},
							},
						},
						{
							Id:          4,
							AuthorId:    []int{1},
							Name:        "Text",
							Sort:        1,
							PublisherId: []int{2},
							Publishers: []Publisher{
								{
									Id:   2,
									Name: "Bar",
								},
							},
						},
					},
				},
			},
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {

			responseAuthor := AuthorsEntitieToResponse(tt.inputData)
			assert.Equal(t, tt.expectedResult, responseAuthor)
		})
	}

}

func TestBooksEnitieToResponse(t *testing.T) {
	testTable := []struct {
		name           string
		inputData      []entities.Book
		expectedResult []Book
	}{
		{
			name: "OK",
			inputData: []entities.Book{
				{
					Id:          1,
					AuthorId:    []int{1},
					Name:        "Text",
					Pages:       10,
					Genres:      []string{"genre1"},
					Sort:        10,
					PublisherId: []int{1, 2},
					Publishers: []*entities.Publisher{
						{
							Id:   1,
							Name: "Foo",
							Sort: 2,
						},
						{
							Id:   2,
							Name: "Bar",
							Sort: 1,
						},
					},
				},
				{
					Id:          2,
					AuthorId:    []int{1},
					Name:        "Text",
					Pages:       10,
					Genres:      []string{"genre2"},
					Sort:        1,
					PublisherId: []int{2},
					Publishers: []*entities.Publisher{
						{
							Id:   2,
							Name: "Bar",
							Sort: 1,
						},
					},
				},
			},
			expectedResult: []Book{
				{
					Id:          1,
					AuthorId:    []int{1},
					Name:        "Text",
					Sort:        10,
					PublisherId: []int{1, 2},
					Publishers: []Publisher{
						{
							Id:   1,
							Name: "Foo",
						},
						{
							Id:   2,
							Name: "Bar",
						},
					},
				},
				{
					Id:          2,
					AuthorId:    []int{1},
					Name:        "Text",
					Sort:        1,
					PublisherId: []int{2},
					Publishers: []Publisher{
						{
							Id:   2,
							Name: "Bar",
						},
					},
				},
			},
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {

			responseAuthor := BooksEnitieToResponse(tt.inputData)
			assert.Equal(t, tt.expectedResult, responseAuthor)
		})
	}

}

func TestPublishersEntitiesToResponse(t *testing.T) {
	testTable := []struct {
		name           string
		inputData      []entities.Publisher
		expectedResult []Publisher
	}{
		{
			name: "OK",
			inputData: []entities.Publisher{
				{
					Id:   1,
					Name: "Text",
					Sort: 10,
				},
				{
					Id:   2,
					Name: "Text",
					Sort: 1,
				},
			},
			expectedResult: []Publisher{
				{
					Id:   1,
					Name: "Text",
				},
				{
					Id:   2,
					Name: "Text",
				},
			},
		},
	}

	for _, tt := range testTable {
		t.Run(tt.name, func(t *testing.T) {

			responseAuthor := PublishersEntitiesToResponse(tt.inputData)
			assert.Equal(t, tt.expectedResult, responseAuthor)
		})
	}

}
