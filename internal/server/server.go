package server

import (
	"context"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"reindexercrud/internal/server/handlers/authorhandler"
	"reindexercrud/internal/server/handlers/bookhandler"
	"reindexercrud/internal/server/handlers/publisherhandler"
	"reindexercrud/pkg/sl"
	"sync"
	"syscall"
	"time"

	_ "reindexercrud/docs"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type Server struct {
	addr   string
	router *gin.Engine
	cache  sync.Map
	log    *slog.Logger
}

func New(addr string, lg *slog.Logger) Server {
	return Server{
		addr:   addr,
		router: gin.Default(),
		log:    lg,
	}
}

func (s *Server) Start(authorUsecase authorhandler.Usecase, bookUsecase bookhandler.Usecase, publisherUsecase publisherhandler.Usecase) {

	//swagger route
	s.router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// adding store for cache and set expiration in 15 minuties
	store := persistence.NewInMemoryStore(time.Minute * 15)

	// author routes
	authorHandler := authorhandler.New(authorUsecase)

	s.router.GET("/authors", authorHandler.GetAuthors)

	// get full info about authors including info about books and publisher
	s.router.GET("/authors/info", authorHandler.GetAuthorsInfo)

	s.router.GET("/authors/:id", cache.CachePage(store, time.Minute*15, authorHandler.GetAuthor))

	// get full info about author including info about books and publisher
	s.router.GET("/authors/:id/info", cache.CachePage(store, time.Minute*15, authorHandler.GetAuthorInfo))
	s.router.POST("/authors", authorHandler.CreateAuthor)
	s.router.PUT("/authors/:id", authorHandler.UpdateAuthor)
	s.router.DELETE("/authors/:id", authorHandler.DeleteAuthor)

	// book routes
	bookHandler := bookhandler.New(bookUsecase)

	s.router.GET("/books", bookHandler.GetBooks)

	// get info about books including info about publishers
	s.router.GET("/books/info", bookHandler.GetBooksInfo)

	s.router.GET("/books/:id", cache.CachePage(store, time.Minute*15, bookHandler.GetBook))

	// get info about book including info about publishers
	s.router.GET("/books/:id/info", cache.CachePage(store, time.Minute*15, bookHandler.GetBookInfo))

	s.router.POST("/books", bookHandler.CreateBook)
	s.router.PUT("/books/:id", bookHandler.UpdateBook)
	s.router.DELETE("/books/:id", bookHandler.DeleteBook)

	// publisher routes
	publisherHandler := publisherhandler.New(publisherUsecase)

	s.router.GET("/publishers", publisherHandler.GetPublishers)
	s.router.GET("/publishers/:id", cache.CachePage(store, time.Minute*15, publisherHandler.GetPublisher))
	s.router.POST("/publishers", publisherHandler.CreatePublisher)
	s.router.PUT("/publishers/:id", publisherHandler.UpdatePublisher)
	s.router.DELETE("/publishers/:id", publisherHandler.DeletePublisher)

	srv := http.Server{
		Addr:    s.addr,
		Handler: s.router,
	}

	s.log.Info("listen server", slog.String("port", s.addr))

	// listen server
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			s.log.Error("faield to serve server", sl.Error(err))
		}
	}()

	// gracefull shutdown server
	ctx, cancel := signal.NotifyContext(context.Background(), os.Kill, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	defer cancel()

	<-ctx.Done()

	ctxShutdown, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	if err := srv.Shutdown(ctxShutdown); err != nil {
		s.log.Error("failed to shutdown server gracefully", sl.Error(err))
		os.Exit(1)
	}

	s.log.Info("closed server gracefully")
}

func CacheHandler(c *gin.Context) {

}
