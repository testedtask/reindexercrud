package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	Env         string `mapstructure:"env"`
	DatabaseURI string `mapstructure:"reindexer_uri"`
	Addr        string `mapstructure:"server_addr"`
}

func InitConfig(path string) (*Config, error) {
	op := "config.InitConfig()"

	v := viper.New()
	v.SetConfigFile(path)
	if err := v.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("%s: failed to read config file: %w", op, err)
	}

	var cfg Config

	if err := v.Unmarshal(&cfg); err != nil {
		return nil, fmt.Errorf("%s: failed to unmarshaled config: %w", op, err)
	}

	return &cfg, nil
}
