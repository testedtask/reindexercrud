package dto

import (
	"reindexercrud/internal/database/models"
	"reindexercrud/internal/entities"
)

func AuthorEntitieToModel(author entities.Author) models.Author {
	return models.Author{
		Id:       author.Id,
		Name:     author.Name,
		LastName: author.LastName,
		BooksId:  append([]int{}, author.BooksId...),
	}
}

func AuthorModelToEntitie(author models.Author) entities.Author {
	var books []*entities.Book

	if len(author.Books) > 0 {
		books = make([]*entities.Book, 0, len(author.Books))

		for _, book := range author.Books {
			entitieBook := BookModelToEntitie(*book)

			books = append(books, &entitieBook)
		}
	}

	return entities.Author{
		Id:       author.Id,
		Name:     author.Name,
		LastName: author.LastName,
		BooksId:  append([]int{}, author.BooksId...),
		Books:    books,
	}
}

func BookEntitieToModel(book entities.Book) models.Book {
	return models.Book{
		Id:          book.Id,
		AuthorId:    book.AuthorId,
		Name:        book.Name,
		Pages:       book.Pages,
		Sort:        book.Sort,
		Genres:      append([]string{}, book.Genres...),
		PublisherId: book.PublisherId,
	}
}

func BookModelToEntitie(book models.Book) entities.Book {
	var publishers []*entities.Publisher

	if len(book.Publishers) > 0 {
		publishers = make([]*entities.Publisher, 0, len(book.Publishers))

		for _, publisher := range book.Publishers {
			entitiePublisher := PublisherModelToEntitie(*publisher)

			publishers = append(publishers, &entitiePublisher)
		}
	}

	return entities.Book{
		Id:          book.Id,
		AuthorId:    book.AuthorId,
		Name:        book.Name,
		Pages:       book.Pages,
		Sort:        book.Sort,
		Genres:      append([]string{}, book.Genres...),
		PublisherId: book.PublisherId,
		Publishers:  publishers,
	}
}

func PublisherEntitieToModel(publisher entities.Publisher) models.Publisher {
	return models.Publisher{
		Id:   publisher.Id,
		Name: publisher.Name,
		Sort: publisher.Sort,
	}
}

func PublisherModelToEntitie(publisher models.Publisher) entities.Publisher {
	return entities.Publisher{
		Id:   publisher.Id,
		Name: publisher.Name,
		Sort: publisher.Sort,
	}
}
